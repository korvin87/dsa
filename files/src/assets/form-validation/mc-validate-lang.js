(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[3]='ANREDE';ftypes[3]='dropdown';fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[5]='INSTITUT';ftypes[5]='text';fnames[4]='TITEL';ftypes[4]='text'; /*
 * Translated default messages for the $ validation plugin.
 * Locale: DE
 */
$.extend($.validator.messages, {
	required: "Dieses Feld ist ein Pflichtfeld.",
	maxlength: $.validator.format("Geben Sie bitte maximal {0} Zeichen ein."),
	minlength: $.validator.format("Geben Sie bitte mindestens {0} Zeichen ein."),
	rangelength: $.validator.format("Geben Sie bitte mindestens {0} und maximal {1} Zeichen ein."),
	email: "Geben Sie bitte eine gultige E-Mail Adresse ein.",
	url: "Geben Sie bitte eine gultige URL ein.",
	date: "Bitte geben Sie ein gultiges Datum ein.",
	number: "Geben Sie bitte eine Nummer ein.",
	digits: "Geben Sie bitte nur Ziffern ein.",
	equalTo: "Bitte denselben Wert wiederholen.",
	range: $.validator.format("Geben Sie bitten einen Wert zwischen {0} und {1}."),
	max: $.validator.format("Geben Sie bitte einen Wert kleiner oder gleich {0} ein."),
	min: $.validator.format("Geben Sie bitte einen Wert gro?er oder gleich {0} ein."),
	creditcard: "Geben Sie bitte ein gultige Kreditkarten-Nummer ein."
});}(jQuery));var $mcj = jQuery.noConflict(true);