/* Google Map */
var gmarkers = [];
var map;
var showAllMarkers = 1; //show all markers on init


var winOptions = {
    content: '',
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-17, -35),
    zIndex: null,
    boxStyle: {
        background: "#fff",
        opacity: 1,
        width: "300px",
    },
    closeBoxMargin: "12px 10px",
    closeBoxURL: "files/src/images/map-close.png",
    infoBoxClearance: new google.maps.Size(20, 20),
    isHidden: false,
    pane: "floatPane",
    enableEventPropagation: false,
};

var infowindow = new InfoBox(winOptions);

//init map
function loadGMap() {
    var mapOptions = {
        center: new google.maps.LatLng(51.071407, 9.854453),
        zoom: 6,
        scrollwheel: false,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {
            position: google.maps.ControlPosition.RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
}
loadGMap();

if (typeof mapAction == 'undefined') {
	mapAction = 'index.php?id=140&type=1454788687&tx_t3map_map[action]=json';
}
$.getJSON(mapAction, function (data) {
    //loadGMap();
    collectItems(data);

    for (var i = 0; i < data.length; i++) {
        addMarker(data[i], i);
    }
    //Clusterizing
    clusterOptions = {
        maxZoom: 12,
        gridSize: 15,
        styles: [{
            url: 'files/src/images/markers/dark-gray.png',
            width: 25,
            height: 30,
            textColor: '#fff',
            textSize: 11,
            anchorText: [-8, 17],
        }],
    }
    var markerCluster = new MarkerClusterer(map, gmarkers, clusterOptions);

    if(!showAllMarkers){
        markerCluster.removeMarkers(gmarkers);
    }else{
        document.querySelector('label.map-show-all').classList.add('active');
    }

    $('.filter-checkbox').change(function () {

        //set map default zoom when choose other category must be before cluster
        map.setZoom(6);

        var filter = []; var prevfilter = [];
        $('.filter-checkbox:checked').each(function (i) {
            filter.push($(this).val());
            prevfilter.push($(this).val());
        });

        if($(this).is(':checked')){
            var index = prevfilter.indexOf($(this).val());
            delete prevfilter[index];

        }else{
            prevfilter.push($(this).val());
            prevfilter.sort();
        }

        if(showAllMarkers){
            prevfilter = [];
            $('.filter-checkbox').each(function (i) {
                prevfilter.push($(this).val());
            })
        }
        showAllMarkers = 0;


        var result = filterMarkers(filter, gmarkers, prevfilter);
        markerCluster.removeMarkers(result['remove']);
        markerCluster.addMarkers(result['add']);

        document.querySelector('label.map-show-all').classList.remove('active');
    })

    document.querySelector('label.map-show-all').addEventListener('click', function(){
        showAllMarkersHandler(showAllMarkers, markerCluster, gmarkers);
        this.classList.add('active');
        showAllMarkers = 1;
    }, false);


    document.querySelector('label.map-show-all').addEventListener('touchstart', function(){
        showAllMarkersHandler(showAllMarkers, markerCluster, gmarkers);
        this.classList.add('active');
        showAllMarkers = 1;
    }, false);

});


/* function show all markers
 *
 * el - button handler label.map-show-all
*/

function showAllMarkersHandler(showAllMarkers, markerCluster, gmarkers){
    if(showAllMarkers)
        return;

    $('.filter-checkbox:checked').each(function (i) {
        $(this).prop('checked', false)
    });
    markerCluster.removeMarkers(gmarkers);
    markerCluster.addMarkers(gmarkers);
}

//call infoWindow by marker id
function gclick(i) {
    google.maps.event.trigger(gmarkers[i], "click");
}

function addMarker(item, i) {
    var category = item.filter;
    var title = item.city;
    var result = item.xy.split(',');
    var pos = new google.maps.LatLng(parseFloat(jQuery.trim(result[0])), parseFloat(jQuery.trim(result[1])));

    var pic = '';
    if (item.img != '' && item.img != undefined) {
        pic = '<div class="infoWin-img"><img class="infoWinPic" width="' + parseInt(winOptions.boxStyle.width) + '"  src="' + item.img + '" alt="' + item.project + '"></div>';
    }
    if (item.link != undefined) {
        var project = '<div class="project-title"><h3 class="infoWin-title"><a href="' + item.link + '">' + item.project + '</a></h3></div>';
        var link = '<div class="infoWin-readmore"><a class="readmore-link " href="' + item.link + '">zum Projekt</a></div>';
    } else {
        var project = '<div class="project-title"><h3 class="infoWin-title">' + item.project + '</h3></div>';
        var link = '';
    }
    var content = '<div class="infoWin" id="infoWin' + i + '"><div class="infoWin-city"><img class="infoWin-marker" src="files/src/images/markers/' + item.markIcon + '">' + item.city + '</div>' + pic + '<div class="infoWin-content">' + project + '<div class="text">' + item.text + '</div>' + link + '</div></div>';

    var marker = new google.maps.Marker({
        title: item.city,
        project: item.project,
        position: pos,
        category: category,
        icon: "files/src/images/markers/" + item.markIcon,
        map: map,
        id: i,
        image:item.img,
    });

    gmarkers.push(marker);

    // Marker click listener
    google.maps.event.addListener(marker, 'click', (function (marker, content) {
        return function () {
            infowindow.setContent(content);
            var img = new Image();
            var imgsrc = marker.get('image');
            if(imgsrc != '' && imgsrc != undefined){
                img.src = imgsrc;
                img.onload = function(){
                    infowindow.open(map, marker);
                }
            }else{
                infowindow.open(map, marker);
            }
        }
    })(marker, content));
}


function filterMarkers(filter, gmarkers, prevfilter) {

    console.log(filter, prevfilter);

    var markerList = [];  //массив видимых фильтров после фильтрации //addmarkers
    var prevList = []; //массив видимых фильтров до фильтрации //removemarkers

    var result = [];

    for (var i = 0; i < gmarkers.length; i++) {

        // If it is the same category or category is not picked
        var arr_category = gmarkers[i].category.split(',');
        for (var k = 0; k < arr_category.length; k++) {
            //markerList
            if (filter.indexOf(arr_category[k]) >= 0) {
                markerList.push(gmarkers[i])
            }
            //prev marker List
            if (prevfilter.indexOf(arr_category[k]) >= 0) {
                prevList.push(gmarkers[i])
            }
        }
    }

    for(var i = 0; i < markerList.length; i++){
        var pi = prevList.indexOf(markerList[i]);
        if(pi >= 0){
            delete markerList[i];
            delete prevList[pi]
        }
    }
    result['add'] = markerList;
    result['remove'] = prevList;


    return result;
}

//collect option items
function collectItems(data){
    //get list of options
    var options = [];
    for (var i = 0; i < data.length; i++) {
        data[i].id = i;

        //create option arrays for markers
        var filter = data[i].filter.split(',');

        if(Array.isArray(filter)){
            for (var l = 0; l < filter.length; l++){
                options[filter[l]] = []
            }
        }else{
            options[filter] = [];
        }
    }

    //collect markers in options
    for (var i = 0; i < data.length; i++) {

        var filter = data[i].filter.split(',');


        if(Array.isArray(filter)){
            for(var l = 0; l < filter.length; l++){
                options[filter[l]].push(data[i]);
            }
        }else{
            options[data[i].filter].push(data[i]);
        }
    }

    //construct html
    for(var k in options){

        var optionItems = options[k];

        //construct counter
        var span = document.createElement('span');
        span.className = 'filter-qty';
        span.innerHTML = '(' + optionItems.length + ')';
        document.getElementById(k).nextElementSibling.appendChild(span)

        //construct container with markers
        var ul = document.createElement('ul');
        ul.className = 'option-container';
        document.getElementById(k).parentElement.appendChild(ul);

        for (var i = 0; i < optionItems.length; i++){
            var li = document.createElement('li');
            li.className = 'school-name';
            li.innerHTML = '<a href="javascript:gclick(' + optionItems[i].id + ')">' + optionItems[i].project + '</a>';
            ul.appendChild(li)
        }

    }
    //return options;


    //add all markers counter
    var span = document.createElement('span');
    span.className = 'filter-qty';
    span.innerHTML = '(' + data.length + ')';
    document.querySelector('label.map-show-all').appendChild(span);
}

//adapt map to screen
function adaptScreen(){
    var winTop = parseInt($(window).scrollTop());
    var top = 0;
    if (winTop > 100) {
        var height = parseInt(window.innerHeight) - parseInt($('header .top-block').height());
        if(height < 480){
            height = 480;
        }
        if(height){
            $('#map_canvas').height(height);
            $('.js-map-interactive').height(height);
            $('.js-map-user-interactions').css('max-height', height + 'px');
        }
    }
    google.maps.event.trigger(map, 'resize');

    console.log('works 1')
}

function adaptFullScreen(){
    var header = document.querySelector('.js-map-interactive > .full-screen-header');
        var headHeight = 0;
        if(header){
            headHeight = parseInt(header.clientHeight);
            var winHeight = parseInt(window.innerHeight);
            var height = winHeight - headHeight;
            if(height){
                $('#map_canvas').height(height);
                $('.js-map-user-interactions').css('max-height', height + 'px');
            }
        }
        google.maps.event.trigger(map, 'resize');

    console.log('works 2')

}

// full screen option
// check map screen state (full width or not)
var mapstate = 0;

$(window).load(function () {
    //open filters
	//open filters
    $('.js-btn-filter').on('click', function(){
        $('.js-mobile-dropdown').toggleClass('closed');
        $(this).toggleClass('open');
    });
    
    $('.js-btn-enlarge').on('click', function(){
        $(this).hide();
        $('.js-btn-minimize').css('display', 'inline-block');
        $('.map-interective-module').addClass('full-screen');
        $('body').addClass('overflow-hidden');
        adaptFullScreen();
        mapstate = 1;
    });
    $('.js-btn-minimize').on('click', function(){
        $(this).hide();
        $('.js-btn-enlarge').css('display', 'inline-block');
        $('.map-interective-module').removeClass('full-screen');
        $('body').removeClass('overflow-hidden');
        adaptScreen();
        mapstate = 0;
    });
    
    adaptScreen();
})


window.addEventListener("orientationchange", function() {
    if(mapstate){
        adaptFullScreen();
    }else{
        adaptScreen();
    }
});

window.addEventListener("resize", function() {
    if(mapstate){
        adaptFullScreen();
    }else{
        adaptScreen();
    }
});







