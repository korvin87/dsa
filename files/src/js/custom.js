var markers = new Array();
var openedInfoWindow;
jQuery(document).ready(function($) {
	
	
	//if($('.bxslider').length){
	//	pagerSlider = $('#bx-pager').bxSlider({
	//		minSlides: 1,
	//		maxSlides: 8,
	//		moveSlides:1,
	//		slideWidth: 64,
	//		slideMargin: 6,
	//		pager:false
	//	});			
	//	slider = $('.bxslider').bxSlider({
	//		startSlide:0,
	//		pagerCustom: '#bx-pager',
	//		onSlideAfter: function($slideElement, oldIndex, newIndex){
	//			//alert($slideElement.html());
	//			$('#img-caption').text($slideElement.find('a').attr('title'));
	//			pagerSlider.goToSlide(newIndex);
	//			$('#bx-pager').find('img').removeClass('active');
	//			$("#bx-pager").find("[data-slide-index='" + newIndex + "']").find('img').addClass('active');
	//			//.addClass('active');
	//		}
	//	});
	//	slider.goToSlide(0);
	//	$('.item-img').click(function(){
	//		$('.item-img').removeClass('active');
	//		$(this).toggleClass('active');
	//		$('#img-caption').text($(this).data('title'));
	//	});		
	//	$('.bxslider .prettyPhoto img').click(function(){
	//		$('#img-caption').text($(this).parent().attr('title'));
	//	});
	//	if($('#img-caption').length){
	//		//$('.item-img:first').trigger('click');
	//	}	
	//	
	//}



	
	$('.plus').click(function(){
		//$($(this).attr('href')).collapse().toggleClass('plus');
		//alert('!');
		//$(this).toggleClass('plus').toggleClass('minus');
		return false;
	});

	$('.has3level').parent().hover(
	  function() {
		$( this ).parent().find( ".level3" ).fadeIn();
		$(this).parent().find('.plus').toggleClass('plus').toggleClass('minus');
	  }, function() {
		$( this ).parent().find( ".level3" ).fadeOut();
		$(this).parent().find('.minus').toggleClass('plus').toggleClass('minus');
	  }
	);	

	$( "li.dropdown" ).hover(
	  function() {
		var deltaX = 0;
		win = $(window).width();
		if(win < 992){
			deltaX = 177;
			w = $('.site-header .container').outerWidth()+3;
		} else {
			deltaX = 202;
			w = $('.site-header .container').outerWidth()+3;
		}
		var offset = $(this).offset();
		var position = $(this).position();
		//alert(position.left+' '+deltaX);
		$( this ).find('.dropdown-menu').fadeIn(300, function(){$(this).addClass( "open" )});
		$( this ).find('.dropdown-menu').css('width', w).css('left',-position.left-deltaX);
	  }, function() {
		$( this ).find('.dropdown-menu').fadeOut(300, function(){$(this).removeClass( "open" )});
		
	  }
	);	
	$( ".top-links a" ).click(
	  function() {
		if($( this ).hasClass( "open" )){
			$( this ).removeClass( "open" );
			$('.top-links-popup').fadeOut();
		} else {
			$( this ).addClass( "open" );
			$('.top-links-popup').fadeIn();			
		}
	  }
	);		

	$('.sitemap').click(function(){
		if($('.footer-form-opened').length){
			$('.footer-form-opened').removeClass('footer-form-opened').animate({ height: 0 }, 900, 'easeOutQuint', function(){$(this).addClass('footer-form-closed')});
		}		
		if($('.site-footer').find('.footer-menu-closed').length){
			var h = $('.footer-menu-closed').find('.container-fluid').height();
			$('.site-footer').find('.footer-menu-closed').animate({ height: h+30 }, 900, 'easeOutQuint', function(){$(this).removeClass('footer-menu-closed').addClass('footer-menu-opened')});
			//addClass('visible').fadeIn(900);
			return false;
		}
		if($('.footer-menu-opened').length){
			$('.footer-menu-opened').removeClass('footer-menu-opened').animate({ height: 0 }, 900, 'easeOutQuint', function(){$(this).addClass('footer-menu-closed')});
			//addClass('hidden');
			return false;
		}		
	});
	
	$('.newsletter').click(function(){
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
		if($('.footer-menu-opened').length){
			$('.footer-menu-opened').removeClass('footer-menu-opened').animate({ height: 0 }, 500, 'easeOutQuint', function(){$(this).addClass('footer-menu-closed')});
		}		
		if($('.site-footer').find('.footer-form-closed').length){
			var h = $('.footer-form-closed').find('.container-fluid').height();
			$('.site-footer').find('.footer-form-closed').animate({ height: h+30 }, 500, 'easeOutQuint', function(){$(this).removeClass('footer-form-closed').addClass('footer-form-opened')});
			//addClass('visible').fadeIn(900);
			return false;
		}
		if($('.footer-form-opened').length){
			$('.footer-form-opened').removeClass('footer-form-opened').animate({ height: 0 }, 500, 'easeOutQuint', function(){$(this).addClass('footer-form-closed')});
			//addClass('hidden');
			return false;
		}		
	});	

	$('.newsletteropen').click(function(){
		if($('.footer-menu-opened').length){
			$('.footer-menu-opened').removeClass('footer-menu-opened').animate({ height: 0 }, 500, 'easeOutQuint', function(){$(this).addClass('footer-menu-closed')});
		}		
		if($('.site-footer').find('.footer-form-closed').length){
			var h = $('.footer-form-closed').find('.container-fluid').height();
			$('.site-footer').find('.footer-form-closed').animate({ height: h+30 }, 500, 'easeOutQuint', function(){$(this).removeClass('footer-form-closed').addClass('footer-form-opened')});
			//addClass('visible').fadeIn(900);
			
		}
		if($('.footer-form-opened').length){
			$('.footer-form-opened').removeClass('footer-form-opened').animate({ height: 0 }, 500, 'easeOutQuint', function(){$(this).addClass('footer-form-closed')});
			//addClass('hidden');
		}
		$('html,body').animate({ scrollTop: $("#footer-newsletter-container").offset().top});
		//$("html, body").animate({ scrollTop: $(document).height() }, 1000);

	});
	
	$("a[rel^='prettyPhoto']").prettyPhoto({
		show_title: true,
		social_tools: '',
		gallery_markup:''
	});

	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.scrollup').fadeIn();
		} else {
			$('.scrollup').fadeOut();
		}		
	});
	$('.scrollup').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});
	
	$('.selectpicker').selectpicker();
	
	$('#select-schulart,#select-bundesland,#select-ganztagsschule').change(function(){
		$('.container_overlay').fadeIn();
		var currentUrl = window.location.pathname;
		//alert('currentUrl='+currentUrl);
		var filterCnt = 0;
		var segmentPrefix = '';
		var segmentSuffix = '';
		// Select country
		var catid = $('#select-schulart :selected').data('cat');
		if(catid == '0'){
			var filterUrl1 = '';
		} else {
			var filterUrl1 = 'tx_news_pi1[overwriteDemand][categories]['+filterCnt+']='+catid;
			//['+filterCnt+']$('.menu-countries').find('#cat-'+catid).attr('href');
			filterCnt++;
			segmentPrefix = '&';
		}
		// Select select-bundesland
		var filterUrl2 = '';
		if($('#select-bundesland').length){
			var catid2 = $('#select-bundesland :selected').data('cat');
			//alert('catid2='+catid2);
			if(catid2 == '0'){
				filterUrl2 = '';
			} else {
				filterUrl2 = segmentPrefix+'tx_news_pi1[overwriteDemand][categories]['+filterCnt+']='+catid2;
				//['+filterCnt+']$('.menu-partner-type').find('#cat-'+catid).attr('href');
				filterCnt++;
				segmentPrefix = '&';
			}
		}
		// Select select-ganztagsschule
		var filterUrl3 = '';
		if($('#select-ganztagsschule').length){
			var catid3 = $('#select-ganztagsschule :selected').data('cat');
			//alert('catid2='+catid2);
			if(catid3 == '0'){
				filterUrl3 = '';
			} else {
				filterUrl3 = segmentPrefix+'tx_news_pi1[overwriteDemand][categories]['+filterCnt+']='+catid3;
				//['+filterCnt+']$('.menu-partner-type').find('#cat-'+catid).attr('href');
				filterCnt++;
				segmentPrefix = '&';
			}
		}
		if(filterCnt > 1){
			segmentSuffix = '&tx_news_pi1[overwriteDemand][categoryConjunction]=and';
		}		
		if(filterCnt == 0){
			filterUrl = currentUrl;
		} else {
			filterUrl = currentUrl + '?' + filterUrl1 + filterUrl2 + filterUrl3 + segmentSuffix;
		}
		//alert(filterUrl);
		$( ".school-items-container" ).load(filterUrl+' .school-items-container', function(responseText, textStatus, XMLHttpRequest){
			//alert(responseText);
			$( ".school-schulart" ).each(function() {
				$( this ).find('.comma:last').addClass('last-comma');
			});			
			$('.container_overlay').fadeOut();
			// !!!loadGMap();
			//window.location = filterUrl;
		});
		//window.location.href = filterUrl;		
	});	
	/*$('a.dropdown-link').off().on('click', function(е){
		e.preventDefault();
		alert('test');
		return false;
	});	*/
	//$('.site-menu').find('a.dropdown-toggle').addClass('dummy');
	/*setTimeout(function() { 
	
	},10000);*/
	
	$( ".school-schulart" ).each(function() {
		$( this ).find('.comma:last').addClass('last-comma');
	});	
	/*$(window).bind("load", function() {
		if($('.tx-pitgooglemaps-pi1').length){
			var num = 1;
			$( ".sidebarMarker" ).each(function() {
				$( this ).prepend('<span class="num">'+num+'</span>');
				num++;
			});
			$( ".sidebarMarker" ).prepend('<span class="top"></span>');
			$( ".sidebarMarker" ).append('<span class="bot"></span>');
			$( ".sidebarMarker:last" ).addClass('last');

		}
	});	*/
	if($("#map_canvas").length){
		google.maps.event.addDomListener(window, 'load', loadGMap); 
	}

});

function loadGMap() {
    var mapOptions = {
		center: new google.maps.LatLng(51.071407, 9.854453),
		zoom: 6,
		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	var cnt = 1;
	var infoWindows = new Array();
	var infowindow = null;
	var markersBounds = new google.maps.LatLngBounds();
	$('.sidebarMarker').each(function() {
		var result = $(this).parent().data('xy').split(',');
		if(result.length == 2){
			var title = $(this).parent().data('address');
			var schoolTitle = $(this).text();
			var teaser = $(this).parent().data('teaser');
			var point = new google.maps.LatLng(parseFloat(jQuery.trim(result[0])),parseFloat(jQuery.trim(result[1])));
			var links = $(this).parent().parent().find('.related-links').html();
			markersBounds.extend(point);
			if(links == undefined){
				links = '';
			}
			infoContent = '<div class="infoWin"><a target="_blank" href="'+$(this).attr('href')+'"><strong>'+schoolTitle+'</strong></a><p>'+teaser+'</p>'+links+'</div>';
			addMarker(map, point, schoolTitle+'\n '+teaser+'\n '+title, infoContent, cnt);
		}
		cnt++;
			
	});
	//map.setCenter(markersBounds.getCenter(), map.fitBounds(markersBounds)); 
}

function addMarker(map, point, title, infoContent, cnt){ 
    marker = new google.maps.Marker({
		position: point,
		icon:"files/src/images/gmarker.png",
		map: map,
		title:title
	});

	//add marker
	marker.setMap(map);

	marker.infowindow = new google.maps.InfoWindow({
		content: infoContent
	});

	//add click event
	google.maps.event.addListener(marker, 'click', function(){
		if (openedInfoWindow != null) openedInfoWindow.close();
		this.infowindow.open(map,this);
		openedInfoWindow = this.infowindow;
	});
	

	
	/*google.maps.event.addListener(marker, 'mouseover', function() {
		this.infowindow.open(map, this);
	}); */
	/*google.maps.event.addListener(marker, 'mouseout', function() {
		this.infowindow.close();
	});*/ 

		
	//add hover event
	$("#marker"+cnt).hover(
		function() {
			google.maps.event.trigger(marker, 'click');
		}, function() {
			setTimeout("marker.infowindow.close();",1)
		}
	);
}

 $(document).ready(function(){
    /* Hier der jQuery-Code */
    $('#btn_cls_nwl').click(function(){
    	$('#msg_bg_nw').hide();
    })
});

 $(document).ready(function(){
    /* Hier der jQuery-Code */
    $('#fe_ttaddress_title_input').mouseover(function(){
    	$('#nwformsubm_div').show('slow');
    })
});

$(document).ready(function(){
    /* Hier der jQuery-Code */
    $('#fe_ttaddress_name_input').mouseover(function(){
    	$('#nwformsubm_div').show('slow');
    })
});

 $(document).ready(function(){
    /* Hier der jQuery-Code */
    $('#fe_ttaddress_first_name_input').mouseover(function(){
    	$('#nwformsubm_div').show('slow');
    })
});

 $(document).ready(function(){
    /* Hier der jQuery-Code */
    $('#fe_ttaddress_first_company_input').mouseover(function(){
    	$('#nwformsubm_div').show('slow');
    })
});


 $(document).ready(function(){
    /* Hier der jQuery-Code */
    $('#fe_ttaddress_email_input').mouseover(function(){
    	$('#nwformsubm_div').show('slow');
    })
});