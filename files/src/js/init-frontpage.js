/* Slick slider functions */

(function(){
    $('.quotes-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: true,    
    });
    
    $('.frontpage-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: true,    
    });
    
    $('.top-sticker-messages').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 7000,
        infinite: true,
        dots: false, 
        arrows: false,
        fade: true,
        adaptiveHeight: true,
        cssEase: 'linear'
    });
})(jQuery)

/* #Slick slider functions */

/* Module Gallery functions */
function gallery_init(defaultwidth){
    var portfolio_height = 'auto';
    var contentWidth    = jQuery(window).width();
    var columnWidth     = defaultwidth;
    var curColCount     = 0;
    var maxColCount     = 0;
    var newColCount     = 0;
    var newColWidth     = 0;
    var win_width = jQuery(window).width();

    curColCount = Math.floor(contentWidth / columnWidth);
    maxColCount = curColCount + 1;
    
    if((maxColCount - (contentWidth / columnWidth)) > ((contentWidth / columnWidth) - curColCount)){
        newColCount = curColCount;
    }
    else{
        newColCount = maxColCount;
    }
    
    newColWidth = contentWidth;

    if(newColCount > 1){
        newColWidth = Math.floor(contentWidth / newColCount);
    }

    jQuery('.gallery-item').width(newColWidth);

    var $container = jQuery('.module-gallery');
    $container.imagesLoaded(function(){
        $container.isotope({
            masonry:{
                columnWidth: newColWidth
            }
        });

    });
}  
(function(){
    var resizeTimer = null;
    $(window).bind('load resize', function() {
       
        var num_items = '6';
       
        var winWidth = $(window).width();
        columnNumber = 6;
        
        if (winWidth > 1200) {
            columnNumber = num_items;
        }else if (winWidth > 800) {
            columnNumber = 4;
        } else if (winWidth > 600) {
            columnNumber = 3;
        } else if (winWidth > 400) {
            columnNumber = 2;
        } else if (winWidth < 399){
            columnNumber = 1;
        }
              
        var gallery_width = jQuery('.gallery').width();
        var item_width = gallery_width/columnNumber;

        if (resizeTimer){
            clearTimeout(resizeTimer);    
        }  
        
        resizeTimer = setTimeout("gallery_init("+item_width+")", 100);
    });   
})(jQuery);


(function(){
	$(window).load(function(){
		/*
		$(".gallery-image-view").prettyPhoto({
				theme: 'pp_default',
				slideshow:5000,
				social_tools: false,
				overlay_gallery: false
		});
		*/		
	})	
})(jQuery)
/* #Module Gallery functions */