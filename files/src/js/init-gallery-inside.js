
/* Gallery Album Slider */
$(window).load(function(){
         $('.album-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            dots: false,
            asNavFor: '.album-thumbs'
        });
        
        $('.album-thumbs').slick({
            slidesToShow: 7,
            slidesToScroll: 1,
            asNavFor: '.album-slider',
            arrow: true,
            dots: false,
            centerMode: true,
            focusOnSelect: true,
              responsive: [
                {
                  breakpoint: 991,
                  settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true,
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                  }
                },
                {
                  breakpoint: 320,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                  }
                },
              ]
        });
        
        $("a[rel^='prettyPhoto']").prettyPhoto({
            theme: 'pp_default',
            slideshow:5000,
            social_tools: false,
            overlay_gallery: false
        });

        $('.album-thumbs').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var text = $(this).find('[data-slick-index=' + nextSlide + ']').find('img').data('title');
            $('#img-caption').text(text);
        });
        
});       
