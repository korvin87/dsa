/*!
 *
 *  Copyright (c) David Bushell | http://dbushell.com/
 *
 */
(function(window, document, undefined)
{

    window.App = (function()
    {

        var _init = false, app = { };

        app.init = function()
        {
            if (_init) {
                return;
            }
            _init = true;

            var nav_open = false;
            $inner = $('#inner-wrap');

            $('#nav-open-btn').on('click', function()
            {
				var w = $( document ).width();
				if(w >= 800){
					menuWidth = '40%'
				}
				if(570 < w && w < 800){
					menuWidth = '60%'
				}
				if(w <= 570){
					menuWidth = '80%'
				}
                if (!nav_open) {
					//$( "body" ).scrollTop( 300 );
					$( "#inner-wrap" ).fadeTo( "slow" , 0.5);
					$("html, body").animate({ scrollTop: 0 }, "slow");
                    //$inner.animate({ right: menuWidth }, 900, 'easeOutQuint').addClass('open');
					$('.site-header').animate({ left: menuWidth }, 900, 'easeOutQuint');
					$('#mob-site-menu').animate({ width: menuWidth }, 900, 'easeOutQuint');
					
//					$('#home-page .fixed').css('right', fixedRight);
					$(this).addClass('menu-opened');
                    nav_open = true;
                    return false;
                } else {
					$( "#inner-wrap" ).fadeTo( "slow" , 1);
                    //$inner.animate({ right: '0' }, 900, 'easeOutQuint').removeClass('open');
					$('.site-header').animate({ left: '0%' }, 900, 'easeOutQuint');
					$('#mob-site-menu').animate({ width: '0%' }, 900, 'easeOutQuint');
//					$('#home-page .fixed').css('right', ($(document).width()-$('#site-content').width())/2);
					$(this).removeClass('menu-opened');
                    nav_open = false;
                    return false;					
				}
				return false;
            });

            //$('#nav-close-btn').on('click', function()
//            {
//                if (nav_open) {
//                    $inner.animate({ left: '0' }, 500);
//                    nav_open = false;
//                    return false;
//                }
//            });
//
            $(document.documentElement).addClass('js-ready');
        };

        return app;

    })();

    $.fn.ready(function()
    {
        window.App.init();
    });

})(window, window.document);
