/* Google Map */
var markers = new Array();
var openedInfoWindow;

(function(){
	if($("#map_canvas").length){
		google.maps.event.addDomListener(window, 'load', loadGMap); 
	}

	function loadGMap() {
		var mapOptions = {
				center: new google.maps.LatLng(51.071407, 9.854453),
				zoom: 6,
				scrollwheel: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
			var cnt = 1;
			var infoWindows = new Array();
			var infowindow = null;
			var markersBounds = new google.maps.LatLngBounds();
			$('.sidebarMarker').each(function() {
				var result = $(this).parent().data('xy').split(',');
				if(result.length == 2){
					var title = $(this).parent().data('address');
					var schoolTitle = $(this).text();
					var teaser = $(this).parent().data('teaser');
					var point = new google.maps.LatLng(parseFloat(jQuery.trim(result[0])),parseFloat(jQuery.trim(result[1])));
					var links = $(this).parent().parent().find('.related-links').html();
					markersBounds.extend(point);
					if(links == undefined){
						links = '';
					}
					infoContent = '<div class="infoWin"><a target="_blank" href="'+$(this).attr('href')+'"><strong>'+schoolTitle+'</strong></a><p>'+teaser+'</p>'+links+'</div>';
					//infoContent = '';

					addMarker(map, point, schoolTitle+'\n '+teaser+'\n '+title, infoContent, cnt);
				}
				cnt++;

			}); 
	}

	function addMarker(map, point, title, infoContent, cnt){ 
		marker = new google.maps.Marker({
			position: point,
			icon:"files/src/images/markers/dark-green.png",
			map: map,
			title:title
		});

		//add marker
		marker.setMap(map);

		marker.infowindow = new google.maps.InfoWindow({
			content: infoContent
		});

		//add click event
		google.maps.event.addListener(marker, 'click', function(){
			if (openedInfoWindow != null) openedInfoWindow.close();
			this.infowindow.open(map,this);
			openedInfoWindow = this.infowindow;
		});


		//add hover event
		$("#marker"+cnt).hover(
			function() {
				google.maps.event.trigger(marker, 'click');
			}, function() {
				setTimeout("marker.infowindow.close();",1)
			}
		);
	}
})(jQuery)
/* #Google Map */