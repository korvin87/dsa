/* Main Menu */
(function($){
    
    function dropdown_menu(){
        if($('.navbar-toggle').is(':hidden') == false){
            $('.dropdown').unbind();
            $('.dropdown-3-level').unbind();
            return;
        }else{ 
        $('.dropdown').on('mouseenter', function (e) {
            $(this).addClass('open');
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).fadeIn(200);
        });		
        $('.dropdown').on('mouseleave', function (e) {
            $(this).removeClass('open');
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).fadeOut(200);
        });

        /* 3rd level */

        $('.dropdown-3-level').on('mouseenter', function (e) {
            $(this).find('.dropdown-3-menu').first().stop(true, true).delay(100).fadeIn(200, function(){
                $(this).parent().addClass('open');
            });
        });		
        $('.dropdown-3-level').on('mouseleave', function (e) {
            $(this).find('.dropdown-3-menu').first().stop(true, true).delay(100).fadeOut(200, function(){
                $(this).parent().removeClass('open');
            });
        });
        }
    }  
    
	$(window).load(function(){
        dropdown_menu();
    })
           
    //delay function
    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
        
    $(window).resize(function(){
        delay(function(){
            dropdown_menu();
        }, 300);
    })
  
	var topH = parseInt($('.top-block').height());
    var brdcrmbsH = parseInt($('.top-block .breadcrumbs-block').height());
    if(!brdcrmbsH){
       brdcrmbsH = 0; 
    }
    var winH = parseInt($(window).height());
 
    $('.mainmenu .navbar-collapse').on('shown.bs.collapse', function () {
        $(this).height(winH - topH + brdcrmbsH);
    })
        
})(jQuery);

/* Top padding for fixed top part */
(function($){
	function topPadding(){
		var h = $('.top-block').height()
		$('header.top').css({paddingTop: h + 'px'})
	}
	$(window).load(function(){
		topPadding();
	})
	$(window).resize(function(){
		topPadding();
	})
	
})(jQuery);

/* Pretty Photo */
$(this).prettyPhoto({
	  theme: 'pp_default',
	  slideshow:5000,
	  social_tools: false,
	  overlay_gallery: false,
	  show_title: true,
	  gallery_markup: '',
});

/* Facebook share link */
(function(){
    $(document).ready(function(){
        var url = window.location.href;
        var title = document.title;
        $('.top-block-soc .facebook-share-link').attr('href', 'http://www.facebook.com/share.php?u=' + url + '&title=' + title)
        .click(function(event){
            event.preventDefault();
            window.open($(this).attr("href"), "popupWindow", "width=600,height=400,scrollbars=yes");
        })
    })
})(jQuery);

/* search field */
(function(){
    $('.top-block-search .search-button').click(function(){
        $(this).siblings('.search-field').toggleClass('active').val('').focus();
    })
})(jQuery);

/* ToTop */
(function($){
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) 
        {
			$('.totop').css({bottom:"25px"});
		} else
        {
			$('.totop').css({bottom:"-100px"});
		}
	})
	$('.totop').click(function(){
		$('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	})    
})(jQuery);

/* #ToTop */

/* Kontakt */
(function(){
    function setPos(){
        var winH = parseInt($(window).height());
        var topH = parseInt($('.top-block').height());
        var btnTop = parseInt($('.contact-btn').width());
        var winScroll = parseInt($(window).scrollTop());
        
        var offset = $('footer').offset();
        if(winScroll > (offset.top - topH))
            winScroll = 100;
        //$('.contact-btn').stop(true, true).animate({marginTop: (winH - topH - btnTop)/2 + topH + winScroll + 'px'}, 800);
        $('.contact-btn').stop(true, true).animate({marginTop: (topH + 50 + winScroll) + 'px'}, 800);
    }
    $(window).load(function(){
        setPos();
    });
    $(window).scroll(function(){
        setPos();
    });
    $(window).resize(function(){
        setPos();
    }); 
})(jQuery)
/* #Kontakt */



/* Header adaptation */

	function adaptHeader(){
		if($('.navbar-toggle:hidden').length){
			var winTop = parseInt($(window).scrollTop());
			if(winTop > 40)
			{
				$('.top-block-item').addClass('scrolled');//for top block	
				$('.mainmenu').addClass('scrolled');//for menu
				$('section.content').addClass('scrolled');//for page slide
			}
			else
			{
				$('.top-block-item').removeClass('scrolled');//for top block	
				$('.mainmenu').removeClass('scrolled');//for menu				
				$('section.content').removeClass('scrolled');//for menu				
			}
		}else{
			$('.top-block-item').removeClass('scrolled');//for top block	
			$('.mainmenu').removeClass('scrolled');//for menu				
			$('section.content').removeClass('scrolled');//for menu			
		}
	}
	
(function($){
		$(window).scroll(function(){
			adaptHeader();
		})
		
		           
    //delay function
    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
        
    $(window).resize(function(){
        delay(function(){
            adaptHeader();
        }, 300);
    })
		
		$(window).load(function(){
			var $window = $(window);
			var scrollTime = 0.35;
			var scrollDistance = 200;

			$window.on("mousewheel DOMMouseScroll", function(event){

				event.preventDefault();	

				var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
				var scrollTop = $window.scrollTop();
				var finalScroll = scrollTop - parseInt(delta*scrollDistance);

				TweenMax.to($window, scrollTime, {
					scrollTo : { y: finalScroll, autoKill:true },
						ease: Power1.easeOut,
						overwrite: 5							
					});

				});
		})
		
})(jQuery)