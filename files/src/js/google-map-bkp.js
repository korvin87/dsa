/* Google Map */
var gmarkers = [];
var map, mapcanvas;
var initMap = false; //������������� �����
var mapOffsetY = 70;
var winCnt = false; //check the window size status

var winOptions = {
    content: '',
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-17, -35),
    zIndex: null,
    boxStyle: {
        background: "#fff",
        opacity: 1,
        width: "300px",
    },
    closeBoxMargin: "12px 10px",
    closeBoxURL: "files/src/images/map-close.png",
    infoBoxClearance: new google.maps.Size(20, 20),
    isHidden: false,
    pane: "floatPane",
    enableEventPropagation: false,
};

var infowindow = new InfoBox(winOptions);

//init map
function loadGMap() {
    var mapOptions = {
    	center: new google.maps.LatLng(51.071407, 9.854453),
        zoom: 6,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_LEFT
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		
		//���������
		var center = map.getCenter();
		var winW = parseInt($(window).width());
		if(winW > 991){
			map.panBy(0, -mapOffsetY);
			winCnt = true;
		}
		map.setCenter(center); 	
}


//����� init map
window.onload = function(){
	var winTop = parseInt($(window).scrollTop());
	if (winTop > 100) {
		initMap = true;
		setTimeout(function(){
			$('#map_canvas').height(parseInt(window.innerHeight) - parseInt($('header .top-block').height()));
			mapBuilder();
		}, 300);
	}
	
	window.onscroll = function(){	
		var winTop = parseInt($(window).scrollTop());
		if (winTop > 100 && !initMap) {
			initMap = true;
			setTimeout(function(){
				$('#map_canvas').height(parseInt(window.innerHeight) - parseInt($('header .top-block').height()));
				mapBuilder();
			}, 300);
		}
	}
}
// �������� � �������
function mapBuilder(){
	if (typeof mapAction == 'undefined') {
		mapAction = 'index.php?id=140&type=1454788687&tx_t3map_map[action]=json';
	}
	$.getJSON(mapAction, function (data) {
		loadGMap();
		for (var i = 0; i < data.length; i++) {
				addMarker(data[i], i);
		}
		//Clusterizing
		clusterOptions = {
				maxZoom: 12,
				gridSize: 15,
				styles: [{
						url: 'files/src/images/markers/dark-gray.png',
						width: 25,
						height: 30,
						textColor: '#fff',
						textSize: 11,
						anchorText: [-8, 17],
				}],
		}
		var markerCluster = new MarkerClusterer(map, gmarkers, clusterOptions);

		$('.filter-checkbox').change(function () {
				var filter = []; var prevfilter = [];
				$('.filter-checkbox:checked').each(function (i) {
						filter.push($(this).val());
						prevfilter.push($(this).val());
				});

				if($(this).is(':checked')){
						var index = prevfilter.indexOf($(this).val());
						delete prevfilter[index];

				}else{
						prevfilter.push($(this).val());
						prevfilter.sort();
				}
            	map.setZoom(6);
            	map.panTo(new google.maps.LatLng(51.071407, 9.854453));

				var result = filterMarkers(filter, gmarkers, prevfilter);
				markerCluster.removeMarkers(result['remove']);
				markerCluster.addMarkers(result['add']);

		})
	});
}


//addmarkers
function addMarker(item, i) {
    var category = item.filter;
    var title = item.city;
    var result = item.xy.split(',');
    var pos = new google.maps.LatLng(parseFloat(jQuery.trim(result[0])), parseFloat(jQuery.trim(result[1])));

    var pic = '';
    if (item.img != '' && item.img != undefined) {
        pic = '<div class="infoWin-img"><img class="infoWinPic" width="' + parseInt(winOptions.boxStyle.width) + '"  src="' + item.img + '" alt="' + item.project + '"></div>';
    }
    if (item.link != undefined) {
        var title = '<div class="project-title"><h3 class="infoWin-title"><a href="' + item.link + '" target="_blank">' + item.project + '</a></h3></div>';
        var link = '<div class="infoWin-readmore"><a class="readmore-link " href="' + item.link + '" target="_blank">zur Website</a></div>';
    } else {
        var title = '<div class="project-title"><h3 class="infoWin-title">' + item.project + '</h3></div>';
        var link = '';
    }
    var content = '<div class="infoWin" id="infoWin' + i + '"><div class="infoWin-city"><img class="infoWin-marker" src="files/src/images/markers/' + item.markIcon + '">' + item.city + '</div>' + pic + '<div class="infoWin-content">' + title + '<div class="text">' + item.text + '</div>' + link + '</div></div>';

    var marker = new google.maps.Marker({
        title: item.city,
        position: pos,
        category: category,
        icon: "files/src/images/markers/" + item.markIcon,
        map: map,
        id: i,
        image:item.img,
    });

    gmarkers.push(marker);

    // Marker click listener
    google.maps.event.addListener(marker, 'click', (function (marker, content) {
        return function () {
            infowindow.setContent(content);
            var img = new Image();
            var imgsrc = marker.get('image');
            if(imgsrc != '' && imgsrc != undefined){
                img.src = imgsrc;
                img.onload = function(){
                    infowindow.open(map, marker);
                }
            }else{
                infowindow.open(map, marker);
            }
        }
    })(marker, content));

    // Marker mouseover listener
    google.maps.event.addListener(marker, 'click', (function (marker, content) {
        return function () {
            var id = marker.get("id");
            if (!document.getElementById('infoWin' + id)) {
                infowindow.setContent(content);
                var img = new Image();
                var imgsrc = marker.get('image');
                if(imgsrc != '' && imgsrc != undefined){
                    img.src = imgsrc;
                    img.onload = function(){
                        infowindow.open(map, marker);
                    }
                }else{
                    infowindow.open(map, marker);
                }
            }
        }
    })(marker, content));
}

function filterMarkers(filter, gmarkers, prevfilter) {

    var markerList = [];  //������ ������� �������� ����� ���������� //addmarkers
    var prevList = []; //������ ������� �������� �� ���������� //removemarkers

    var result = [];

    for (var i = 0; i < gmarkers.length; i++) {

        // If it is the same category or category not picked
        var arr_category = gmarkers[i].category.split(',');
        for (var k = 0; k < arr_category.length; k++) {
            //markerList
            if (filter.indexOf(arr_category[k]) >= 0) {
                markerList.push(gmarkers[i])
            }
            //prev marker List
            if (prevfilter.indexOf(arr_category[k]) >= 0) {
                prevList.push(gmarkers[i])
            }
        }
    }

    for(var i = 0; i < markerList.length; i++){
        var pi = prevList.indexOf(markerList[i]);
        if(pi >= 0){
            delete markerList[i];
            delete prevList[pi]
        }
    }
    result['add'] = markerList;
    result['remove'] = prevList;

    return result;
}

//filter switcher
(function ($) {
    $(window).load(function () {
        $('.map-filter-switcher').click(function () {
            $(this).fadeOut(200);
            $('.map-module-filter').fadeIn(250);
        });
        $('.map-module-filter .filter-close').click(function () {
            $('.map-module-filter').fadeOut({
                duration: 200,
                done: function () {
                    $('.map-filter-switcher').fadeIn(250);
                }
            });
        })
    })
})(jQuery);



// ����� 
google.maps.event.addDomListener(window, "resize", function() {
	$('#map_canvas').height(parseInt(window.innerHeight) - parseInt($('header .top-block').height()));
	var winW = parseInt($(window).width());
	var center = map.getCenter();
	google.maps.event.trigger(map, "resize");
	map.setCenter(center); 
	if(winW > 991 && !winCnt){
		map.panBy(0, -mapOffsetY);
		winCnt = true;
	}
	if(winW < 992 && winCnt){
		map.panBy(0, mapOffsetY);
		winCnt = false;
	}
});