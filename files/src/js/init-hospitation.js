$('#select-schulart,#select-bundesland,#select-ganztagsschule').change(function(){
		$('.container_overlay').fadeIn();
		var currentUrl = window.location.pathname;
		//alert('currentUrl='+currentUrl);
		var filterCnt = 0;
		var segmentPrefix = '';
		var segmentSuffix = '';
		// Select country
		var catid = $('#select-schulart :selected').data('cat');
		if(catid == '0'){
			var filterUrl1 = '';
		} else {
			var filterUrl1 = 'tx_news_pi1[overwriteDemand][categories]['+filterCnt+']='+catid;
			//['+filterCnt+']$('.menu-countries').find('#cat-'+catid).attr('href');
			filterCnt++;
			segmentPrefix = '&';
		}
		// Select select-bundesland
		var filterUrl2 = '';
		if($('#select-bundesland').length){
			var catid2 = $('#select-bundesland :selected').data('cat');
			//alert('catid2='+catid2);
			if(catid2 == '0'){
				filterUrl2 = '';
			} else {
				filterUrl2 = segmentPrefix+'tx_news_pi1[overwriteDemand][categories]['+filterCnt+']='+catid2;
				//['+filterCnt+']$('.menu-partner-type').find('#cat-'+catid).attr('href');
				filterCnt++;
				segmentPrefix = '&';
			}
		}
		// Select select-ganztagsschule
		var filterUrl3 = '';
		if($('#select-ganztagsschule').length){
			var catid3 = $('#select-ganztagsschule :selected').data('cat');
			//alert('catid2='+catid2);
			if(catid3 == '0'){
				filterUrl3 = '';
			} else {
				filterUrl3 = segmentPrefix+'tx_news_pi1[overwriteDemand][categories]['+filterCnt+']='+catid3;
				//['+filterCnt+']$('.menu-partner-type').find('#cat-'+catid).attr('href');
				filterCnt++;
				segmentPrefix = '&';
			}
		}
		if(filterCnt > 1){
			segmentSuffix = '&tx_news_pi1[overwriteDemand][categoryConjunction]=and';
		}		
		if(filterCnt == 0){
			filterUrl = currentUrl;
		} else {
			filterUrl = currentUrl + '?' + filterUrl1 + filterUrl2 + filterUrl3 + segmentSuffix;
		}
		//alert(filterUrl);
		$( ".school-items-container" ).load(filterUrl+' .school-items-container', function(responseText, textStatus, XMLHttpRequest){
			//alert(responseText);
			//$( ".school-schulart" ).each(function() {
			//	$( this ).find('.comma:last').addClass('last-comma');
			//});			
			$('.container_overlay').fadeOut();
			// !!!loadGMap();
			//window.location = filterUrl;
		});
		//window.location.href = filterUrl;		
	});	