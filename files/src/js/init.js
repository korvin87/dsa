


/* Main Menu */
(function ($) {

    function dropdown_menu() {
        if ($('.navbar-toggle').is(':hidden') == false) {
            $('.dropdown').unbind();
            $('.dropdown-3-level').unbind();
            return;
        } else {
            $('.dropdown').on('mouseenter', function (e) {
                $(this).addClass('open');
                $(this).find('.dropdown-menu').first().stop(true, true).delay(100).fadeIn(200);
            });
            $('.dropdown').on('mouseleave', function (e) {
                $(this).removeClass('open');
                $(this).find('.dropdown-menu').first().stop(true, true).delay(250).fadeOut(200);
            });

            /* 3rd level */

            $('.dropdown-3-level').on('mouseenter', function (e) {
                $(this).find('.dropdown-3-menu').first().stop(true, true).delay(100).fadeIn(200, function () {
                    $(this).parent().addClass('open');
                });
            });
            $('.dropdown-3-level').on('mouseleave', function (e) {
                $(this).find('.dropdown-3-menu').first().stop(true, true).delay(100).fadeOut(200, function () {
                    $(this).parent().removeClass('open');
                });
            });
        }
    }

    $(window).load(function () {
        dropdown_menu();
    })

    //delay function
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(window).resize(function () {
        delay(function () {
            dropdown_menu();
        }, 300);
    })

    var topH = parseInt($('.top-block').height());
    var brdcrmbsH = parseInt($('.top-block .breadcrumbs-block').height());
    if (!brdcrmbsH) {
        brdcrmbsH = 0;
    }
    var winH = parseInt($(window).height());

    $('.mainmenu .navbar-collapse').on('shown.bs.collapse', function () {
        $(this).height(winH - topH + brdcrmbsH);
    })

})(jQuery);

/* Top padding for fixed top part */
(function ($) {
    function topPadding() {
        var h = $('.top-block').height()
        $('header.top').css({paddingTop: h + 'px'})
    }

    $(window).load(function () {
        topPadding();
    })
    $(window).resize(function () {
        topPadding();
    })

})(jQuery);

/* Pretty Photo */
/*
    $(this).prettyPhoto({
        theme: 'pp_default',
        slideshow: 5000,
        social_tools: false,
        overlay_gallery: false,
        show_title: true,
        gallery_markup: '',
    });

*/
/* Facebook share link */
(function () {
    $(document).ready(function () {
        var url = window.location.href;
        var title = document.title;
        $('.top-block-soc .facebook-share-link').attr('href', 'http://www.facebook.com/share.php?u=' + url + '&title=' + title)
            .click(function (event) {
                event.preventDefault();
                window.open($(this).attr("href"), "popupWindow", "width=600,height=400,scrollbars=yes");
            })
    })
})(jQuery);

/* search field */
(function () {
    $('.top-block-search .search-button').click(function () {
        $(this).siblings('.search-field').toggleClass('active').val('').focus();
    })
})(jQuery);

/* ToTop */
(function ($) {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.totop').css({bottom: "25px"});
        } else {
            $('.totop').css({bottom: "-100px"});
        }
    })
    $('.totop').click(function () {
        $('html, body').animate({scrollTop: '0px'}, 800);
        return false;
    })
})(jQuery);

/* #ToTop */

/* Kontakt */
(function () {
    function setPos() {
        var winH = parseInt($(window).height());
        var topH = parseInt($('.top-block').height());
        var btnTop = parseInt($('.contact-btn').width());
        var winScroll = parseInt($(window).scrollTop());

        var offset = $('footer').offset();
        if (winScroll > (offset.top - topH))
            winScroll = 100;
        //$('.contact-btn').stop(true, true).animate({marginTop: (winH - topH - btnTop)/2 + topH + winScroll + 'px'}, 800);
        $('.contact-btn').stop(true, true).animate({marginTop: (topH + 50 + winScroll) + 'px'}, 800);
    }

    $(window).load(function () {
        setPos();
    });
    $(window).scroll(function () {
        setPos();
    });
    $(window).resize(function () {
        setPos();
    });
})(jQuery)
/* #Kontakt */



/* Header adaptation */

function adaptHeader() {
    if ($('.navbar-toggle:hidden').length) {
        var winTop = parseInt($(window).scrollTop());
        if (winTop > 40) {
            $('.top-block-item').addClass('scrolled');//for top block
            $('.mainmenu').addClass('scrolled');//for menu
            $('section.content').addClass('scrolled');//for page slide
        }
        else {
            $('.top-block-item').removeClass('scrolled');//for top block
            $('.mainmenu').removeClass('scrolled');//for menu
            $('section.content').removeClass('scrolled');//for menu
        }
    } else {
        $('.top-block-item').removeClass('scrolled');//for top block
        $('.mainmenu').removeClass('scrolled');//for menu
        $('section.content').removeClass('scrolled');//for menu
    }
}


/* T3 Popups */
(function () {
    $(document).ready(function () {
    	setTimeout(function () {
            //if no form on page
            if ($('.powermail_form, form.validate').length == 0) {
                $.get( "/index.php?type=1472290667", function( data ) {
                    $('body').append(data);
                    $('#events .event-container').each(function() {
                        var id = $(this).data('id');
                        var cookie = getCookie('popup-' + id);
                        if (cookie) {
                            $('#events .event-container-' + $(this).data('id')).remove();
                        }	    			
                    });
                    $('.icon-close').click(function(){
                        $(this).parents('.event-container')
                        var el = $(this).parents('.event-container');
                        $(el).removeClass('active').delay(350).queue(function(){$(this).addClass('remove');});	                
                        setCookie('popup-' + $(el).data('id'), 'true', 365);
                    })
                    animateItems(3000);    	    
                });
            }
    	}, 20000); // 20 Sec.
    });
    
    function animateItems(duration) {
        $('#events .event-container').each(function (i, item) {
            setTimeout(function () {
                $(item).addClass('active');
            }, i * duration); // this multiplies the index by the duration so the handler is fired in sequence
        });
    }
    
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length,c.length);
            }
        }
        return "";
    }
})(jQuery);

/* News cloud */

$(document).ready(function(){
	
	
	if ( $( "#news-tags" ).length ) {
		var sortedNewsTags = [];
		newsTags.forEach(function(entry) {
			if (entry.weight > 0) {
				sortedNewsTags.push(entry);
			}
		});
		if (sortedNewsTags.length > 5) {
		    
			$( "#news-tags" ).jQCloud(sortedNewsTags);
			$( "#news-tags" ).show();
		}
	}	
});


/* Teams map */
(function ($) {

    var west = ['DE-NW', 'DE-RP', 'DE-SL'],
    east = ['DE-BE', 'DE-BB', 'DE-ST', 'DE-TH', 'DE-SN'],
    north = ['DE-NI', 'DE-HB', 'DE-HH', 'DE-SH', 'DE-MV'],
    south = ['DE-HE', 'DE-BW', 'DE-BY'],
    initColors = {
      'initial': {
        'west': "#8dccbe",
        'east': "#005e45",
        'north': "#007f67",
        'south': "#4baf94"
      },
      'hover': "#e42322",
      'selected': "#e42322",
      'selectedHover': "#e42322"
    },
    generateColors = function (selectRegion) {
      var colors = {},
        region,
        code;
      selectRegion = selectRegion || '';
      if (selectRegion != '') {
        for (code in map.regions) {
          if (selectRegion == whichRegion(code)) {
            colors[code] = initColors.selected;
          }
          else {
            colors[code] = initColors.initial[whichRegion(code)];
          }
        }
      }
      else {
        for (code in map.regions) {
          region = whichRegion(code);
          if (region != 'undefined') {
            colors[code] = initColors.initial[whichRegion(code)];
          }
        }
      }
      return colors;
    },
    whichRegion = function(code) {
      if(west.indexOf(code) !== -1) {
        return 'west';
      }
      else if (east.indexOf(code) !== -1) {
        return 'east';
      }
      else if (north.indexOf(code) !== -1) {
        return 'north';
      }
      else if (south.indexOf(code) !== -1) {
        return 'south';
      }
      else if (code == 'DE-NORTH') {
        return 'north';
      }
      else if (code == 'DE-SOUTH') {
        return 'south';
      }
      else if (code == 'DE-EAST') {
        return 'east';
      }
      else if (code == 'DE-WEST') {
        return 'west';
      }
    },
    map;

  var initMarkers = [
    /*EAST*/
    {name: 'DE-BE', coords: [52.57944414473308, 13.346861084449127]},
    {name: 'DE-BE', coords: [52.47944414473308, 13.346861084449127]},
    {name: 'DE-BB', coords: [51.8999491761363, 13.734805946708999]},
    {name: 'DE-BB', coords: [51.7999491761363, 13.734805946708999]},
    {name: 'DE-ST', coords: [51.8999491761363, 11.545688509671146]},
    {name: 'DE-ST', coords: [51.7999491761363, 11.545688509671146]},
    {name: 'DE-TH', coords: [51.018478862618916, 10.797509132455678]},
    {name: 'DE-TH', coords: [50.918478862618916, 10.797509132455678]},
    {name: 'DE-SN', coords: [51.07080095870558, 13.069757611406361]},
    {name: 'DE-SN', coords: [50.97080095870558, 13.069757611406361]},
    /*WEST*/
    {name: 'DE-NW', coords: [51.55629946412796, 7.610819192463868]},
    {name: 'DE-NW', coords: [51.45629946412796, 7.610819192463868]},
    {name: 'DE-RP', coords: [50.066570167274985, 7.19516398289972]},
    {name: 'DE-RP', coords: [49.966570167274985, 7.19516398289972]},
    {name: 'DE-SL', coords: [49.45735691291207, 6.890350162552676]},
    {name: 'DE-SL', coords: [49.35735691291207, 6.890350162552676]},
    /*NORTH*/
    {name: 'DE-HB', coords: [53.18213605666723, 8.774653779243486]},
    {name: 'DE-HB', coords: [53.08213605666723, 8.774653779243486]},
    {name: 'DE-NI', coords: [52.39361783855271, 10.021619407935933]},
    {name: 'DE-NI', coords: [52.29361783855271, 10.021619407935933]},
    {name: 'DE-SH', coords: [54.21681842225632, 9.661384892980339]},
    {name: 'DE-SH', coords: [54.11681842225632, 9.661384892980339]},
    {name: 'DE-HH', coords: [53.62868132817907, 10.021619407935933]},
    {name: 'DE-HH', coords: [53.52868132817907, 10.021619407935933]},
    {name: 'DE-MV', coords: [53.84201855523513, 12.737233443755041]},
    {name: 'DE-MV', coords: [53.74201855523513, 12.737233443755041]},
    /*SOUTH*/
    {name: 'DE-BY', coords: [49.09542216385332, 11.76737128810536]},
    {name: 'DE-BY', coords: [48.99542216385332, 11.76737128810536]},
    {name: 'DE-BW', coords: [48.69422880996604, 8.996336557677699]},
    {name: 'DE-BW', coords: [48.59422880996604, 8.996336557677699]},
    {name: 'DE-HE', coords: [50.720871807450965, 8.996336557677699]},
    {name: 'DE-HE', coords: [50.620871807450965, 8.996336557677699]}
  ];

  $(document).ready(function () {
      if($('#teams-map').length > 0) {
        map = new jvm.Map({
          container: $('#teams-map'),
          map: 'de_merc',
          backgroundColor: "#fff",
          zoomOnScroll: false,
          regionsSelectable: true,
          regionsSelectableOne: true,
          regionStyle: {
            hover: {
              "fill-opacity": 1,
              cursor: 'pointer',
              fill: initColors.hover
            },
            selected: {
              fill: initColors.selected
            },
            selectedHover: {
              fill: initColors.selectedHover
            }
          },
          markerStyle: {
            initial: {
              fill: '#fff',
              stroke: 'transparent',
              "fill-opacity": 1,
              "stroke-width": 0,
              "stroke-opacity": 0,
              r: 2
            },
            hover: {
              stroke: 'transparent',
              "stroke-width": 0
            },
            selected: {
              fill: '#fff'
            },
            selectedHover: {}
          },
          series: {
            regions: [{
              attribute: 'fill'
            }]
          },
          onRegionClick: function(e, code) {
            var region = whichRegion(code);        
            map.series.regions[0].setValues(generateColors(region));
            href = $('*[data-map-code="' + code + '"]').attr('href');
            window.location.href = href;
            e.preventDefault();
          },
          onRegionOver: function(e, code) {
            $('[data-map-code="'+ code +'"]').toggleClass('hover');
          },
          onRegionOut: function(e, code) {
            $('[data-map-code="'+ code +'"]').toggleClass('hover');
          },
          markers: initMarkers.map(function (h) {
            return {name: h.name, latLng: h.coords}
          })
        });

        /*Set initial colors*/
        map.series.regions[0].setValues(generateColors());
      }

    $('.map-nav-item')
      /*Select region on the map to show hovered region*/
      .hover(
      function () {
        var code = $(this).data('mapCode');
        var obj = {};        
        for (var regionCode in map.regions) {
            if (code == regionCode) {
                obj[regionCode] = true;
            }
            else {
                obj[regionCode] = false;
            }
        }
        if (code == 'DE-NORTH') {
            for (var index in north){
            	obj[north[index]] = true;
        	}
       	}
        if (code == 'DE-SOUTH') {
            for (var index in south){
            	obj[south[index]] = true;
        	}
       	}
        if (code == 'DE-WEST') {
            for (var index in west){
            	obj[west[index]] = true;
        	}
       	}
        if (code == 'DE-EAST') {
            for (var index in east){
            	obj[east[index]] = true;
        	}
       	}
          
          
        map.setSelectedRegions(obj);
      },
      function () {
        var obj = {};
        for (var regionCode in map.regions) {
          obj[regionCode] = false;
        }
        map.setSelectedRegions(obj);
      })
      /*Fill all region area (team) as selected*/
      .on('click', function (e) {
       // e.preventDefault();
      var code = $(this).data('mapCode');
      var region = whichRegion(code);
      map.series.regions[0].setValues(generateColors(region));
    });

  });
    
})(jQuery);

(function ($) {
    $(window).scroll(function () {
        adaptHeader();
    })


    //delay function
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(window).resize(function () {
        delay(function () {
            adaptHeader();
        }, 300);
    });

    //termine call
    $(window).load(function(){
        $('.termine-call').click(function(){
            $('#termine').addClass('active');
        });
        $('.termine-close').click(function(){
            $('#termine').removeClass('active');
        })
    })

})(jQuery)

//termine scroll

//можно отключить gsap/TweenMax.min.js, gsap/ScrollToPlugin,
// в принципе я не видел где использовали jquery.mousewheel еще тоже можно отключить

document.addEventListener('DOMContentLoaded', function(event){

  var elem = document.querySelector('.termine-container');
  if (elem.addEventListener) {
    if ('onwheel' in document) {
      elem.addEventListener("wheel", onWheel);
    } else if ('onmousewheel' in document) {
      elem.addEventListener("mousewheel", onWheel);
    } else {
      elem.addEventListener("MozMousePixelScroll", onWheel);
    }
  } else {
    elem.attachEvent("onmousewheel", onWheel);
  }

  function onWheel(e) {
    e = e || window.event;

    var delta = e.deltaY || e.detail || e.wheelDelta;

    if (delta < 0 && this.scrollTop == 0) {
      e.preventDefault();
    }

    if (delta > 0 && this.scrollHeight - this.clientHeight - this.scrollTop <= 1) {
      e.preventDefault();
    }
  }
});



$(document).ready(function(){
    if(window.location.hash){
        if(window.location.hash == '#news-filter'){
            console.log($('.news-filter-buttons').offset().top);
            setTimeout(function(){
            	$('html, body').animate({ scrollTop: $('.news-filter-buttons').offset().top - 300 + 'px' });
               }, 600); 
        }
    }
    $('.map-nav-item').each(function(){
       if($(this).data('active')){
        $(this).click();
       }
    });
});


$(document).ready(function(){
	var selectedEvents = [];
	
	function refreshSelectedEvents() {
		selectedEvents = [];
		$('.powermail-event').each(function() {
			var currentValue = $(this).val();
			if (currentValue != '') {
				selectedEvents.push(currentValue);
			}
		});	
	}
	
	function hideSelectedEvents() {
		$('.powermail-event').each(function() {
			var selectValue = $(this).val();
			$(this).find('option').each(function() {
				var optionValue = $(this).val();
				if (optionValue != selectValue) {
					console.log(selectedEvents);
					console.log(selectedEvents.indexOf(optionValue));				
					if (selectedEvents.indexOf(optionValue) > -1) {
						$(this).hide();
					} else {
						$(this).show();
					}
				}
			});			
		});
	}
	$('.powermail-event').change(function() {
		refreshSelectedEvents();
		hideSelectedEvents();
	});
});
var checkedPowermailEmails = [];
var registredEmails = [];
$(function () {
	var formID = $('.powermail_form').data('id');
	var formPID = $('.powermail_form').data('form-pid');
	var savePid = $('.powermail_form').data('save-pid');
	if (!savePid) {
		savePid = formPID;
	}
	if (formID) {
		$.getJSON('?type=1485695721&tx_t3powermailevents_pi1[form]=' + formID + '&tx_t3powermailevents_pi1[action]=list&tx_t3powermailevents_pi1[pid]=' + savePid, function(data) {
			$.each( data, function( key, val ) {
				registredEmails.push( val );
			});
		});
	}
	window.Parsley.addValidator('custom105', {		
		validateString: function(value) {
			var email = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
			if (email.test(value)) {
				
				if (registredEmails.indexOf(value) > -1) {
					if (!(checkedPowermailEmails.indexOf(value) > -1 )) {
						checkedPowermailEmails.push(value);
						var popupContent = 'Die E-Mail-Adresse ' + value + ' wurde bereits für eine Anmeldung zu dieser Veranstaltung verwendet. Bitte prüfen Sie, ob Sie bereits angemeldet sind';
						alert(popupContent);
					} else {
						return true;
					}
				} else {
					return true;
				}
				
			} else {
				return false;
			}
	  	},
		messages: {
			en: 'Error',
			de: 'Fehler'
		}
	});
});

(function () {
	$( 'audio' ).audioPlayer();
})(jQuery);

(function () {
	
	function readCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}
	
	function createCookie (name, value, days, path, domain, secure) {
	  if (days) {
	        var date = new Date();
	        date.setTime(date.getTime()+(days*24*60*60*1000));
	        var expires = date.toGMTString();
	    }
	    else var expires = "";
	    document.cookie = name + "=" + escape(value) +
	        ((days) ? "; expires=" + expires : "") +
	        ((path) ? "; path=" + path : "") +
	        ((domain) ? "; domain=" + domain : "") +
	        ((secure) ? "; secure" : "");
	}
	
	$(document).ready(function(){
		  if (readCookie('acceptGoogle') == null) {
			    if ($('.t3-cookie-notice').length) {
			      $('.t3-cookie-notice').show();
			    }
			  }
			  $('#acceptGoogle').click(function(e) {
			    e.preventDefault();
			    createCookie('acceptGoogle', '1', 300, '/');
			    $('.t3-cookie-notice').slideToggle();			    
			  });
	});
})(jQuery);
